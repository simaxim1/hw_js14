'use strict'

const btn = document.querySelector('button');
const cssLink = document.querySelector('link');

const styleOne = "./css/style1.css";
const styleTwo = "./css/style2.css";

let actualStyle = localStorage.getItem('css');
actualStyle === null ? cssLink.setAttribute('href', styleOne) : cssLink.setAttribute('href', actualStyle)

btn.addEventListener('click', () => {
    
    if (cssLink.getAttribute('href') === styleOne) {
        cssLink.setAttribute('href', styleTwo);
        localStorage.setItem('css', styleTwo);
    } else {
        cssLink.setAttribute('href', styleOne)
        localStorage.setItem('css', styleOne);
    }
    
    cssLink.href = localStorage.css;
});